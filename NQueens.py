#!/usr/bin/env python
# a0.py : Solve the N-Rooks/N-Queen problem!
# D. Crandall, 2016
# Rashmi Bidanta 08/31/2017


import sys

# Prints the board in a graphically pleasing format
def show_board(board,problem_type):
    return "\n".join([ " ".join([ "_" if col==0 else "Q" if col==1 and problem_type == \
     "nqueen" else "R" if col==1 and problem_type == "nrook" else col for col in row ]) \
      for row in board])

# This method counts the number of pieces on the board at current provided state
# board
def count_pieces(board):
    return sum([sum(filter(lambda x: isinstance(x,int),row)) for row in board])

def count_row_pieces(board,row):
    return sum( filter(lambda x: isinstance(x,int),board[row]))

# This method places a piece on the board at the provided position and returns
# the board
def place_piece(board, row_position, col_position):
    board[row_position][col_position] = 1;
    return board

# This method removes a piece on the board at the provided position and returns
# the board
def remove_piece(board, row_position, col_position):
    board[row_position][col_position] = 0;
    return board

# Checks if the a Piece placed at position row_num,col_num has any conflict with
# any other piece in the same row,column or diagonals
def check_position_validity(board,problem_type,row_num,col_num):
    n = len(board)
    validity = True
    if board[row_num][col_num] == "X":
        return False
    for i in range(col_num):
        if board[row_num][i]==1:
            validity = False
    for i in range(row_num):
        if board[i][col_num]==1:
            validity = False
    if problem_type == "nqueen":
        for rows in reversed(range(row_num)):
            for cols in reversed(range(col_num)):
                if rows - cols == row_num - col_num and board[rows][cols] == 1:
                    validity = False
        for rows in reversed(range(row_num)):
            for cols in range(col_num+1,n):
                if rows + cols == row_num + col_num and board[rows][cols] == 1:
                    validity = False
    return validity

positions = []
# Solves the board for Queens:
def solve_queens(board,row,N):
    if row == N:
        return board
    for col in range(N):
        if check_position_validity(board,problem_type,row,col):
            positions.append((row,col))
            place_piece(board,row,col)
            if row == N:
                break
            else:
                solve_queens(board,row+1,N)
    if count_row_pieces(board,row) == 0:
        if len(positions) > 0:
            prev = positions.pop()
            remove_piece(board,prev[0],prev[1])

    return board

# Solves the board for rooks
def solve_rooks(board,problem_type):
    lastposition = ((),)
    for row in range(N):
        for col in range(N):
            if check_position_validity(board,problem_type,row,col):
                place_piece(board,row,col)
                lastposition = (row,col)
                break
        if count_row_pieces(board,row) == 0:
            remove_piece(board,lastposition[0],lastposition[1])
            place_piece(board,lastposition[0],lastposition[1]+1)
            place_piece(board,row,lastposition[1])
    return board


args = []
expected_args = [1,2,3,4]

for item in sys.argv[1:]:
    args.append(item)

args_recieved = dict(zip(expected_args,args))

# Problem Type (N-rook/N-queen)
problem_type = args_recieved[1]
# Size of the Board
N = int(args_recieved[2])
# Position of Dead Cell initializing it to (0,0) assuming matrix is 1-indexed
dead_row_position = 0
dead_col_position = 0

if 3 and 4 in args_recieved:
    dead_row_position = int(args_recieved[3])
    dead_col_position = int(args_recieved[4])

given_board =  ([ [ "X" if (rows == dead_row_position-1 and cols == dead_col_position-1)
                    else 0 for cols in range(N)] for rows in range(N)])

if problem_type == "nqueen":
    print (show_board(solve_queens(given_board,0,N),problem_type))
elif problem_type == "nrook":
    print (show_board(solve_rooks(given_board,problem_type),problem_type))
else:
    print "Please enter valid arguments"
